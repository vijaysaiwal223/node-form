// Import Modules
const fs = require("fs");
const http = require("http");
const url = require("url");

const replaceTempate = require('./modules/replaceTemplate')

// ---------------- FILES ---------------//

// Reading Files Synchronously //
const textIn = fs.readFileSync("./txt/input.txt", "utf-8");
// console.log(textIn);

// Writing Files Synchronously //
const textOut = `This is what we know about avocado: ${textIn}. \n Created On ${Date()}`;
// fs.writeFileSync("./txt/output.txt", textOut);
// console.log("file gonna be written");

// Reading Files Asynchoronously //
fs.readFile("./txt/start.txt", "utf-8", (err, data) => {
  fs.readFile(`./txt/${data}.txt`, "utf-8", (err, data1) => {
    // console.log(data1);
    fs.readFile(`./txt/append.txt`, "utf-8", (err, data2) => {
        // console.log(data2);

      // Writting Files Asynchoronusly //
      fs.writeFile("./txt/final.txt", `${data1}\n${data2}`, "utf-8", (err) => {
        // console.log("file has been written!");
      });
    });
  });
});
// console.log("will read file");

// ---------------- SERVERS ---------------//

// replace template function

//Template-Oeriview
const tempOverview = fs.readFileSync(
  `${__dirname}/templates/template-overview.html`,
  "utf-8"
);

//Template-Card
const tempCard = fs.readFileSync(
  `${__dirname}/templates/template-card.html`,
  "utf-8"
);

// Template-product
const tempProduct = fs.readFileSync(
  `${__dirname}/templates/template-product.html`,
  "utf-8"
);

// JSON data
const data = fs.readFileSync(`${__dirname}/dev-data/data.json`, "utf-8");
const dataObj = JSON.parse(data);

const server = http.createServer((req, res) => {
  const { query, pathname } = url.parse(req.url, true);

  // Overview page
  if (pathname === "/" || pathname === "/overview") {
    res.writeHead(200, {
      "Content-type": "text/html",
    });

    const cardHtml = dataObj.map((el) => replaceTempate(tempCard, el)).join("");
    const output = tempOverview.replace("{%PRODUCT_CARDS%}", cardHtml);

    res.end(output);
    // console.log(cardHtml);

    // Product page
  } else if (pathname === "/product") {
    const product = dataObj[query.id];
    const output = replaceTempate(tempProduct, product);

    res.end(output);

    // API
  }
  if (pathname === "/api") {
    res.writeHead(200, {
      "Content-type": "application/json",
    });
    res.end(data);

    // Page Not Found
  } else {
    res.writeHead(404, {
      "Content-type": "text/html",
    });
    res.end("<h1>404 You Lost baby</h1>");
  }
});

server.listen(8080, () => {
  console.log("Listening to localhost on post 8080");
});
